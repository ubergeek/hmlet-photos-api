# hmlet-photos-api

A backend application that exposes a REST API for uploading and managing Photos. Written in DRF and Django.

This is a take home assignment prepared for an interview.

Project scrum board: <https://gitlab.com/zach-assignments/hmlet-photos-api/-/boards>

Go straight to demo: <https://demo-hmlet-assignment.herokuapp.com/>

- Demo instance is hosted on a free dyno on heroku and is prone to sleeping after 30 mins of inactivity.
- Uploaded photo images are stored in AWS S3 bucket.
- There no network optimizations or CDN/caching implementations therefore responses will be slow.

## Table of contents

- [Features Overview](#features-overview)
- [Interacting with Demo API](#interacting-with-demo-api)
  1. [Authentication](#authentication)
  2. [Permissions](#permissions)
- [Setup Project on Local Environment](#setup-project-on-local-environment)
- [Design considerations](#design-considerations)
- [Application Settings (e.g. settings.py)](#application-settings-eg-settingspy)
  1. [JWT](#jwt)
- [Third-party Tools, Frameworks, and Libraries](#third-party-tools-frameworks-and-libraries)

## Features Overview

Features specified in the Take Home Assignment PDF. For implementation details go to [Design Choices](#design-choices) section.

- Post a photo :white_check_mark:
- Save photos as draft :white_check_mark:
- Edit photo captions :white_check_mark:
- Delete photos :white_check_mark:
- List photos (all, my photos, my drafts) :white_check_mark:
- ASC/DESC Sort photos on publishing date :white_check_mark:
- Filter photos by user :white_check_mark:
- Limit the uploaded photo size to a certain maximum dimensions and bytes :no_entry: (Due to implementation of bonus features.)
- JWT authentication :white_check_mark:
- Host it somewhere like Heroku or PythonAnywhere.com, both offer free hosting for python apps :white_check_mark:
- Maintain a good git history :white_check_mark:

### Bonus features

- Remove the dimension/size limit, and store the original photo, but serve only proportionally resized/cropped photos based on pre-defined dimensions :white_check_mark:
- Implement batch upload, edit, delete, publish API for photos :tractor:
- Support #tags in captions, and filtering on the same :tractor:

## Interacting with Demo API

**Browsable API** - On your browser, go to <https://demo-hmlet-assignment.herokuapp.com/>.

**HTTP Client** - Make http calls to <https://demo-hmlet-assignment.herokuapp.com/> using a HTTP client such as [Postman app](https://www.getpostman.com/).

### Authentication

---

#### Using the _Browsable API_

You may log in via the button on the top right hand corner on the _Browsable API_. This uses the `rest_framework.authentication.SessionAuthentication` class.

#### Using a _HTTP Client_

You may only access authenticated user permissions via JWT authentication. This uses the `rest_framework_simplejwt.authentication.JWTAuthentication` class from [Django Rest Framework - Simple JWT](https://github.com/davesque/django-rest-framework-simplejwt)

Steps as follows:

1. Perform **POST** `/auth/token/` with the following JSON request body:

```json
{
  "username": "<some_user>",
  "password": "<password>"
}
```

2. You'll receive a JSON response.

```json
{
  "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTU3OTMyOTYxMiwianRpIjoiNmM4NTNjNDM0NTM4NDI0ODlhYmM4MzQzOTk3NzQzNzQiLCJ1c2VyX2lkIjoxfQ.XWU1j64_egcNlCGM1D-IpK-rNHK7U3O___oGSNeSVAg",
  "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTc5MjQzNTEyLCJqdGkiOiJjZDdmNDI2M2I5MWI0YmYzYWIyNzk5YjgwZDUzOWM5MyIsInVzZXJfaWQiOjF9.pm-okiBOvmBjDD_xBpgLFygMfdcTY2YeIz7Ww_WMLRw"
}
```

Use the `access` token on the `Authorization` header with type `Bearer` in all your HTTP requests to the API. E.g. `Authorization: Bearer <access_token_here>`

3.  To refresh. Perform **POST** `/auth/token/refresh/` with the refresh token in your JSON request body like this:

```json
{
  "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTU3OTMyOTYxMiwianRpIjoiNmM4NTNjNDM0NTM4NDI0ODlhYmM4MzQzOTk3NzQzNzQiLCJ1c2VyX2lkIjoxfQ.XWU1j64_egcNlCGM1D-IpK-rNHK7U3O___oGSNeSVAg"
}
```

**Access token lifetime** is set to **5 minutes**, and **refresh token lifetime** is set to **1 day** as per library defaults [(see JWT settings)](#jwt).

After 5 minutes from the time that access token is issued, it will expire. Perform step 3 to get a new access token.

#### Test Users

You may use the below test users for authentication or create your own users via django-admin `/admin/` interface.

| username | password | is_superuser |
| :------- | :------- | :----------: |
| admin    | Hmlet123 |     True     |
| zachpang | Hmlet123 |    False     |
| testuser | Hmlet123 |    False     |

### Permissions

---

As an **unauthenticated user**, you can ONLY perform GET calls on the API such as:

- **GET** `/photos/`
  1. Listing all published photos by **all registered users** on the system. Published photos are determined by **is_draft** property that are set by the owners of the photo
  2. Filtering via `/photos/?is_draft=true` is ignored.
  3. Ordering by **published** date (e.g. `/photos/?ordering=-published` for DESC order)

- **GET** `/photos/all/`
  1. Listing all published photos by **all registered users** on the system.
  2. Filtering published photos via **username** `/photos/all/?owner__username={username}`
  3. Ordering by **published** date (e.g. `/photos/?ordering=-published` for DESC order)

- **GET** `/photos/{photo_id}/`
  1. Retrieves the photo object by the **photo_id** param

As an **authenticated user** (See [authentication](#authentication)) you can:

- **GET** `/photos/`

  1. Listing all photos owned by the **authenticated user** on the system. Both published and draft are shown unless **is_draft** query param is included.
  2. Filtering via **is_draft** query param. To view 'published only' (`/photos/?is_draft=false`), 'draft only' (`/photos/?is_draft=true`)
  3. Ordering by **published** date (e.g. `/photos/?ordering=-published` for DESC order)

- **POST** `/photos/`

  1. Create a photo and persist in the database. **title**, **caption**, **is_draft** are optional fields. **Image** field is mandatory.

- **GET** `/photos/all/` - Same permission as **unauthenticated user**

- **GET** `/photos/{photo_id}/` - Same permission as **unauthenticated user**

- **PATCH** `/photos/{photo_id}/`
  1. Replacing **title**, **caption**, **is_draft** on the photo owned by the **authenticated user** can be done through **PATCH**.

Take note **PUT** request is prevented intentionally as it requires a full request body, which includes the **image** field.

This prevents the user from unintentionally replacing the image on the photo object. To perform a similar operation, the current object can be deleted before posting another photo.

- **DELETE** `/photos/{photo_id}/`
  1. Delete the photo owned by the **authenticated user**.

## Setup Project on Local Environment

### 1. Make sure you are running python 3.8 and above. Use [pyenv](https://github.com/pyenv/pyenv) to manage python versions

```zsh
user@user-machine some-working-dir % pyenv global 3.8.0
```

### 2. Clone the project into your working directory.

```zsh
user@user-machine some-working-dir % git clone https://gitlab.com/zach-assignments/hmlet-photos-api.git
user@user-machine some-working-dir % cd hmlet-photos-api
```

### 3. Create virtualenv in the project directory. Activate the virtualenv, and install dependencies.

```zsh
user@user-machine hmlet-photos-api % python -m venv .venv
user@user-machine hmlet-photos-api % source .venv/bin/activate
(.venv) user@user-machine hmlet-photos-api % pip install -r requirements.txt
```

**Alternatively**, if you have **[poetry](https://python-poetry.org/)** installed:

```zsh
user@user-machine hmlet-photos-api % poetry install
```

As configured in [poetry.toml](./poetry.toml), poetry will set up a virtualenv directory **.venv**, and install dependencies listed in **[pyproject.toml](./pyproject.toml)** into the **.venv**.

Activtate the virtualenv

```zsh
user@user-machine hmlet-photos-api % poetry shell
```

### 4. Create **.env** file in the project root, following the example in **[.env.example](./.env.example)**

```conf
SECRET_KEY='you can generate a secret key from django.core.management.utils.get_random_secret_key()'
DEBUG=True
ALLOWED_HOSTS='localhost', '127.0.01.1', '[::1]'

# Make sure you have postgres running with the `hmlet_assignment` database
# and `app_user` user created and granted permissions on the database.
# Comment/remove this line for database config to default to sqlite
DATABASE_URL='postgres://app_user:<password>@localhost:5432/hmlet_assignment'

USE_AWS_S3=False

# You need to provide these credentials to an S3 bucket if USE_AWS_S3 is True
AWS_ACCESS_KEY_ID=''
AWS_SECRET_ACCESS_KEY=''
AWS_STORAGE_BUCKET_NAME='hmlet-assignment-photos-bucket'
AWS_DEFAULT_ACL='public-read'
```

### 5. Run **_migrate_** to create the necessary tables in the database.

```zsh
(.venv) user@user-machine hmlet-photos-api % python manage.py migrate
```

Additionally, create some users through the django-admin interactive shell or **createsuperuser** command

### 6. Run server.

```zsh
(.venv) user@user-machine hmlet-photos-api % python manage.py runserver

# OR using gunicorn
(.venv) user@user-machine hmlet-photos-api % gunicorn config.wsgi
```

### 7. Finally, go to `localhost:8000` on your browser

## Design Considerations

Under

## Application Settings (e.g. settings.py)

### JWT

Uses library defaults

```python
SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(minutes=5),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=1),
    ...
    ...
}
```

## Third-party Tools, Frameworks, and Libraries

- [Pyenv](https://github.com/pyenv/pyenv) - Python version management. Easily switch between mutliple versions of Python
- [Poetry](https://python-poetry.org/) - Dependency management.

Brief overview of the third-party dependencies chosen for the development of this project

Snippet taken from the project's **[pyproject.toml](./pyproject.toml)**:

```toml
[tool.poetry.dependencies]
python = "~3.8"
django = "^3.0.1"                               # Main web application development framework
djangorestframework = "^3.11.0"                 # Extensions for developing REST APIs on Django
pillow = "^7.0.0"                               # Python Imaging Library (PIL) for manipulating image data/files
djangorestframework-simplejwt = "^4.4.0"        # JSON Web Token authentication backend for the Django REST Framework
django-filter = "^2.2.0"                        # Generic reusable django backend for filtering querysets
python-decouple = "^3.3"                        # Separation of environment-sepcific settings from code
psycopg2 = "^2.8.4"                             # PostgreSQL database adaptor for Python
dj-database-url = "^0.5.0"                      # Parses a Database URL into a Django database connection dictionary
gunicorn = "^20.0.4"                            # WSGI HTTP Server. Standard recommendation by Django and Heroku for production use
whitenoise = "^5.0.1"                           # Solution for effective use of app server for serving compressed static assets in production
django-storages = "^1.8"                        # A collection Django storage backends for storing files with cloud storage providers
boto3 = "^1.11.3"                               # Python library for connecting and managing AWS services

[tool.poetry.dev-dependencies]
black = "^19.10b0"                              # Opinionated Python code formatter
isort = "^4.3.21"                               # Imports sorting formatter
```
