"""
WSGI config for hmlet_project project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os
import sys
from pathlib import Path

from django.core.wsgi import get_wsgi_application

# Configuring web server to include and serve files/apps in hmlet_project filepath.
# This allows easy placement of apps within the interior
# hmlet_project directory.
project_root = Path(__file__).resolve().parent.parent
sys.path.append(str(project_root.joinpath('hmlet_project')))

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')

application = get_wsgi_application()
