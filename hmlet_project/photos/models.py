from django.db import models


def user_directory_original_path(instance, filename):
    return f"user_{instance.owner.id}/origin/{filename}"


def user_directory_thumbnail_path(instance, filename):
    return f"user_{instance.owner.id}/{filename}"


class AutoNowPublishDateTimeField(models.DateTimeField):
    """
    Checks the model instance is_draft status before applying auto_now on date field
    """

    def pre_save(self, model_instance, add):
        is_draft = getattr(model_instance, "is_draft", True)

        if is_draft:
            setattr(model_instance, self.attname, None)
            return None
        else:
            self.auto_now = True

        return super().pre_save(model_instance, add)


class Photo(models.Model):
    # TODO: ordering by pub date, filtering by user

    title = models.CharField(max_length=100, blank=True)
    caption = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    image = models.ImageField(upload_to=user_directory_original_path)
    thumbnail = models.ImageField(
        upload_to=user_directory_thumbnail_path, editable=False
    )
    owner = models.ForeignKey(
        "auth.User", on_delete=models.CASCADE, related_name="photos"
    )

    # Bad practice to use status flags,
    # but in order to allow a checkbox/trigger UI on browsable api
    # and also prevent manual selection of publish date
    #
    # Ideally, the frontend client should be managing inputs, while backend does the validation.
    is_draft = models.BooleanField(default=True)
    published = AutoNowPublishDateTimeField(null=True, editable=False)

    def __str__(self):
        return 'id: %s, title: "%s", owner_id: %s' % (
            self.id,
            self.title,
            self.owner.id,
        )
