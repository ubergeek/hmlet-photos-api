import io
import os

from django.core.files.base import ContentFile

from PIL import Image


class ImageService:
    @classmethod
    def create_thumbnail_from_uploaded_file(cls, uploaded_image_file):
        """
        Create thumbnail from image file
        """
        file_name, file_extension = os.path.splitext(uploaded_image_file.name)
        pil_image = Image.open(uploaded_image_file)
        pil_image.thumbnail((400, 400))

        thumb_bytesIO = io.BytesIO()

        try:
            # we save the image in the same format as the original image
            # however, there is a chance that pillow opens read-only formats
            # but are unable to save/write them
            pil_image.save(thumb_bytesIO, pil_image.format)
        except IOError:
            # in that case, ignore thumbnail for unsupported formats
            return None

        thumb_name = file_name + ".thumb" + file_extension
        thumb_file = ContentFile(thumb_bytesIO.getvalue(), name=thumb_name)

        return thumb_file
