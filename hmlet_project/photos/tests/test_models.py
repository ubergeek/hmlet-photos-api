import datetime
import os

from django.conf import settings
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase

from photos.models import Photo


class PhotoModelTests(TestCase):
    # TODO: consider mocking instead of making db calls
    def setUp(self):
        self.user = User.objects.create_user(
            "test_user", "test@test.com", password="test_password"
        )

    def test_create_photo_instance(self):
        """
        Photo can be instantiated and saved in db
        """
        title = "Test title"
        caption = "Test captions"

        photo = Photo(title=title, caption=caption, owner_id=self.user.id)
        photo.save()

        retrieved_photo = Photo.objects.get(title=title)

        self.assertEqual(retrieved_photo, photo)
        self.assertIsNot(retrieved_photo, photo)

    def test_create_photo_instance_draft(self):
        """
        Photo instance should have is_draft True, and published date set to None
        """
        title = "Test title"
        caption = "Test captions"

        photo = Photo(title=title, caption=caption, owner_id=self.user.id)
        photo.save()

        retrieved_photo = Photo.objects.get(title=title)

        self.assertEqual(retrieved_photo, photo)
        self.assertIsNot(retrieved_photo, photo)
        self.assertEqual(retrieved_photo.is_draft, True)
        self.assertIs(retrieved_photo.published, None)

    def test_create_photo_instance_not_draft(self):
        """
        Photo instance should have is_draft False, and published date set
        """
        title = "Test title"
        caption = "Test captions"
        is_draft = False

        photo = Photo(
            title=title, caption=caption, owner_id=self.user.id, is_draft=is_draft
        )
        photo.save()

        retrieved_photo = Photo.objects.get(title=title)

        self.assertEqual(retrieved_photo, photo)
        self.assertIsNot(retrieved_photo, photo)
        self.assertEqual(retrieved_photo.is_draft, False)
        self.assertIsNotNone(retrieved_photo.published)
        self.assertIsInstance(retrieved_photo.published, datetime.datetime)

    def test_update_draft_to_False_should_nullify_published_date(self):
        """
        Photo that was published should nullify date when it is set to draft
        """
        title = "Test title"
        caption = "Test captions"
        is_draft = False

        # instance is saved immediately on create()
        Photo.objects.create(
            title=title, caption=caption, owner_id=self.user.id, is_draft=is_draft
        )

        retrieved_photo = Photo.objects.get(title=title)
        self.assertEqual(retrieved_photo.is_draft, False)
        self.assertIsInstance(retrieved_photo.published, datetime.datetime)

        retrieved_photo.is_draft = True
        retrieved_photo.save()

        self.assertEqual(retrieved_photo.is_draft, True)
        self.assertIs(retrieved_photo.published, None)

    def test_photo_str_representation(self):
        """
        Photo has a human-readable representation
        """
        test_id = 1
        title = "Some title"

        photo = Photo.objects.create(title=title, owner_id=self.user.id)

        self.assertEqual(photo.__str__(), 'id: 1, title: "Some title", owner_id: 1')

    def test_photo_with_file(self):
        """
        Photo with uploaded file
        """
        file_1 = SimpleUploadedFile(
            "test_1.jpeg", b"some content", content_type="image/jpeg"
        )
        file_2 = SimpleUploadedFile(
            "test_2.png", b"some content", content_type="image/png"
        )

        photo_1 = Photo.objects.create(image=file_1, owner_id=self.user.id)
        photo_2 = Photo.objects.create(image=file_2, owner_id=self.user.id)

        self.assertIsInstance(photo_1, Photo)
        self.assertIsInstance(photo_2, Photo)

        # clean up dummy files and dir
        self.addCleanup(os.remove, os.path.join(settings.BASE_DIR, photo_2.image.path))
        self.addCleanup(os.remove, os.path.join(settings.BASE_DIR, photo_1.image.path))
