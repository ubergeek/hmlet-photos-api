from unittest import mock

from django.contrib.auth.models import AnonymousUser, User
from django.test import TestCase

from photos import permissions


class PhotoPermissionTests(TestCase):
    def create_mock_obj(self):
        mock_obj = mock.Mock()
        mock_obj.owner = User.objects.create_user("TestOwner", password="abc123TEST")
        return mock_obj

    def setUp(self):
        self.permission_class = permissions.IsOwnerOrReadOnly()
        self.mock_obj = self.create_mock_obj()
        self.mock_view = mock.Mock()
        self.owner = User.objects.get(username="TestOwner")
        self.non_owner = User.objects.create_user("AnotherUser", password="abc123TEST")
        self.anonymous = AnonymousUser()

    def test_IsOwnerOrReadOnly_has_object_permission_GET(self):
        """
        Owner, non-owners, anonymous can GET
        """
        mock_request = mock.Mock()
        mock_request.method = "GET"

        # Test for owner
        mock_request.user = self.owner
        has_obj_permission = self.permission_class.has_object_permission(
            mock_request, self.mock_view, self.mock_obj
        )
        self.assertTrue(has_obj_permission)

        # Test for owner
        mock_request.user = self.non_owner
        has_obj_permission = self.permission_class.has_object_permission(
            mock_request, self.mock_view, self.mock_obj
        )
        self.assertTrue(has_obj_permission)

        # Test for anonymous user
        mock_request.user = self.anonymous
        has_obj_permission = self.permission_class.has_object_permission(
            mock_request, self.mock_view, self.mock_obj
        )

        self.assertTrue(has_obj_permission)

    def test_IsOwnerOrReadOnly_has_object_permission_PUT(self):
        """
        Only Owner can PUT
        """
        mock_request = mock.Mock()
        mock_request.method = "PUT"

        # Test for owner
        mock_request.user = self.owner
        has_obj_permission = self.permission_class.has_object_permission(
            mock_request, self.mock_view, self.mock_obj
        )

        self.assertTrue(has_obj_permission)

        # Test for non-owner
        mock_request.user = self.non_owner
        has_obj_permission = self.permission_class.has_object_permission(
            mock_request, self.mock_view, self.mock_obj
        )
        self.assertFalse(has_obj_permission)

        # Test for anonymous user
        mock_request.user = self.anonymous
        has_obj_permission = self.permission_class.has_object_permission(
            mock_request, self.mock_view, self.mock_obj
        )

        self.assertFalse(has_obj_permission)

    def test_IsOwnerOrReadOnly_has_object_permission_PATCH(self):
        """
        Only Owner can PATCH
        """
        mock_request = mock.Mock()
        mock_request.method = "PATCH"

        # Test for owner
        mock_request.user = self.owner
        has_obj_permission = self.permission_class.has_object_permission(
            mock_request, self.mock_view, self.mock_obj
        )

        self.assertTrue(has_obj_permission)

        # Test for non-owner
        mock_request.user = self.non_owner
        has_obj_permission = self.permission_class.has_object_permission(
            mock_request, self.mock_view, self.mock_obj
        )
        self.assertFalse(has_obj_permission)

        # Test for anonymous user
        mock_request.user = self.anonymous
        has_obj_permission = self.permission_class.has_object_permission(
            mock_request, self.mock_view, self.mock_obj
        )

        self.assertFalse(has_obj_permission)

    def test_IsOwnerOrReadOnly_has_object_permission_DELETE(self):
        """
        Only Owner can Delete
        """
        mock_request = mock.Mock()
        mock_request.method = "DELETE"

        # Test for owner
        mock_request.user = self.owner
        has_obj_permission = self.permission_class.has_object_permission(
            mock_request, self.mock_view, self.mock_obj
        )

        self.assertTrue(has_obj_permission)

        # Test for non-owner
        mock_request.user = self.non_owner
        has_obj_permission = self.permission_class.has_object_permission(
            mock_request, self.mock_view, self.mock_obj
        )
        self.assertFalse(has_obj_permission)

        # Test for anonymous user
        mock_request.user = self.anonymous
        has_obj_permission = self.permission_class.has_object_permission(
            mock_request, self.mock_view, self.mock_obj
        )

        self.assertFalse(has_obj_permission)
