from django.contrib.auth.models import AnonymousUser
from rest_framework import filters, permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from django_filters import rest_framework as df

from photos.models import Photo
from photos.permissions import IsOwnerOrReadOnly
from photos.serializers import PhotoSerializer
from photos.services import ImageService


class PhotoViewSet(viewsets.ModelViewSet):
    """
    For authenticated users:
    - GET /photos/ provides `list`, with filtering based on `is_draft` query param, for the user's own photo objects.
    - POST /photos/ provides `create`.
    - GET, PATCH, DELETE /photos/{id} provides `retrieve`,`update` and `destroy` actions for the user's own photo objects.
    
    For unauthenticated users:
    - GET /photos/ provides `list` on all published photos in the system.

    For all users:
    Additionally, we provide an extra `all` action.
    - GET /photos/all provides `list` on all published photos, with filtering based on `owner__username`.
    
    ASC/DESC ordering by published date on GET /photos and GET /photos/all
    Defaults to DESC
    """

    serializer_class = PhotoSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

    filter_backends = [df.DjangoFilterBackend, filters.OrderingFilter]
    filterset_fields = ["is_draft"]
    ordering_fields = ["published"]
    ordering = ["-published"]  # specify default ordering to be desc

    def get_queryset(self):
        """
        The default list method in this ModelViewSet should use this queryset,
        displaying only photos for the currently authenticated user.

        An anonymous user should be able to view all published photos only
        """
        user = self.request.user

        if isinstance(user, AnonymousUser):
            return Photo.objects.filter(is_draft=False)

        return Photo.objects.filter(owner=self.request.user)

    def perform_create(self, serializer):
        """
        Override perform_create() in CreateModelMixin.

        - Create thumbnail image
        - Associate owner field to user
        """
        # It is not necessary to do try/except, check for None
        # As request parameters are already validated by the serializer at this point
        uploaded_file = self.request.data.get("image", None)

        thumbnail = ImageService.create_thumbnail_from_uploaded_file(uploaded_file)

        # This is the standard practice to inject additional data at point of creating instance
        # https://www.django-rest-framework.org/api-guide/serializers/#saving-instances
        serializer.save(owner=self.request.user, thumbnail=thumbnail)

    def update(self, request, *args, **kwargs):
        """
        Override update() in UpdateModelMixin.

        Updates to image file is not allowed.
        Therefore, updates via PUT request is not allowed, 
        since a PUT request requires all fields of the model instance json to be populated
        """
        partial = kwargs.get("partial", False)

        if not partial:
            error_obj = {
                "errors": [
                    "Updates via PUT request will not proceed becausing replacing 'image' is not allowed.",
                    "Only updates to 'title', 'caption', and 'is_draft' via PATCH request are allowed.",
                ]
            }

            return Response(error_obj, status=status.HTTP_405_METHOD_NOT_ALLOWED)

        return super().update(request, *args, **kwargs)

    def partial_update(self, request, *args, **kwargs):
        """
        Override partial_update() in UpdateModelMixin.

        Updates to image file is not allowed.
        Only updates to 'title', 'caption', 'is_draft' will pass through
        """

        # Block update calls that replaces the 'image' field with a new image
        image_file = request.data.get("image", False)

        if image_file:
            error_obj = {"image": ["Updates to image file is not allowed."]}
            return Response(error_obj, status=status.HTTP_400_BAD_REQUEST)

        return super().partial_update(request, *args, **kwargs)

    @action(methods=["get"], detail=False, filterset_fields=["owner__username"])
    def all(self, request, *args, **kwargs):
        queryset = self.filter_queryset(Photo.objects.filter(is_draft=False))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
