#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys
from pathlib import Path


def main():
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc

    # When executing scripts through manage.py, append hmlet_project dir to sys.path
    # This allows easy access to apps within the interior
    # hmlet_project project directory.
    project_root = Path(__file__).resolve().parent
    sys.path.append(str(project_root.joinpath("hmlet_project")))

    execute_from_command_line(sys.argv)


if __name__ == "__main__":
    main()
